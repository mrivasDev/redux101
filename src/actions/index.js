let nextTodoId = 0
export const addTodo = (text) => {
  console.log('ADD_TODO')
  return {
    type: 'ADD_TODO',
    id: nextTodoId++,
    text
  }
}

export const setVisibilityFilter = (filter) => {
  console.log('SET_VISIBILITY_FILTER')
  return {
    type: 'SET_VISIBILITY_FILTER',
    filter
  }
}

export const toggleTodo = (id) => {
  console.log('TOGGLE_TODO')
  return {
    type: 'TOGGLE_TODO',
    id
  }
}

export const deleteTask = (id) => {
  console.log('DELETE_TASK')
  return {
    type: 'DELETE_TASK',
    id
  }
}